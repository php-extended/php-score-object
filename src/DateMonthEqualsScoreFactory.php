<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

use DateTimeInterface;

/**
 * DateMonthEqualsScoreFactory class file.
 * 
 * This class is a factory that builds DateMonthEqualsScore objects.
 * 
 * @author Anastaszor
 */
class DateMonthEqualsScoreFactory implements ScoreFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Score\ScoreFactoryInterface::createScore()
	 * @param array<integer|string, ?DateTimeInterface> $values
	 * @psalm-suppress ImplementedParamTypeMismatch
	 */
	public function createScore(array $values = []) : ScoreInterface
	{
		$values = \array_values($values);
		
		return new DateMonthEqualsScore($values[0] ?? null, $values[1] ?? null);
	}
	
}
