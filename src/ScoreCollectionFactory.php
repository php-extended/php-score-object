<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * ScoreCollectionFactory class file.
 * 
 * This class is a factory that builds ScoreCollection objects.
 * 
 * @author Anastaszor
 */
class ScoreCollectionFactory implements ScoreCollectionFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Score\ScoreCollectionFactoryInterface::createScoreFactory()
	 */
	public function createScoreCollection() : ScoreCollectionInterface
	{
		return new ScoreCollection();
	}
	
}
