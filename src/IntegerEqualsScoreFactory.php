<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * IntegerEqualsScoreFactory class file.
 * 
 * This class is a factory that builds IntegerEqualsScore objects.
 * 
 * @author Anastaszor
 */
class IntegerEqualsScoreFactory implements ScoreFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Score\ScoreFactoryInterface::createScore()
	 */
	public function createScore(array $values = []) : ScoreInterface
	{
		$values = \array_values($values);
		/** @phpstan-ignore-next-line */
		$expected = isset($values[0]) && \is_scalar($values[0]) ? (int) $values[0] : null;
		/** @phpstan-ignore-next-line */
		$actual = isset($values[1]) && \is_scalar($values[1]) ? (int) $values[1] : null;
		
		return new IntegerEqualsScore($expected, $actual);
	}
	
}
