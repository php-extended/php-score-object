<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * CounterScoreFactory class file.
 * 
 * This class is a factory that builds CounterScore objects.
 * 
 * @author Anastaszor
 */
class CounterScoreFactory implements ScoreFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Score\ScoreFactoryInterface::createScore()
	 */
	public function createScore(array $values = []) : ScoreInterface
	{
		$values = \array_values($values);
		/** @phpstan-ignore-next-line */
		$count = isset($values[0]) && \is_scalar($values[0]) ? (int) $values[0] : 0;
		
		return new CounterScore($count);
	}
	
}
