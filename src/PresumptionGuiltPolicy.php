<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * PresumptionGuiltPolicy class file.
 *
 * This score is made to absorb multiple scores and merge them into an audible
 * score value using the "presumption of guilt" strategy (meaning values
 * that scored for zero are counted as such).
 *
 * @author Anastaszor
 */
class PresumptionGuiltPolicy implements ScorePolicyInterface
{
	
	/**
	 * The minimal value.
	 * 
	 * @var float
	 */
	protected float $_min = 0.0;
	
	/**
	 * The maximal value.
	 * 
	 * @var float
	 */
	protected float $_max = 0.0;
	
	/**
	 * The current value.
	 * 
	 * @var float
	 */
	protected float $_current = 0.0;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScorePolicyInterface::absorb()
	 */
	public function absorb(ScoreInterface $score, int $weight = 1) : bool
	{
		$this->_max += (float) $weight;
		$this->_current += ((float) $weight * $score->getNormalizedValue());
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScorePolicyInterface::getCurrentValue()
	 */
	public function getCurrentValue() : ScoreInterface
	{
		if(0.0 === $this->_max)
		{
			return new FloatScore(0, 1, 0);
		}
		
		return new FloatScore($this->_min, $this->_max, $this->_current);
	}
	
}
