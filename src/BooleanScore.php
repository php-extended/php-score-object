<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * EqualityScore class file.
 *
 * This class is a generic comparator between two objects that scores if the
 * two objects are equals. This score is equal to 100% if both the expected and
 * the actual values are equals, and is equal to 0% if both values are not
 * equals for the sense of the === operator.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.NumberOfChildren")
 */
class BooleanScore implements ScoreInterface
{	
	/**
	 * Whether the score is valid.
	 * 
	 * @var boolean
	 */
	protected $_valid = false;
	
	/**
	 * Builds a new BooleanScore with the given validity.
	 * 
	 * @param boolean $valid
	 */
	public function __construct(bool $valid)
	{
		$this->_valid = $valid;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return (string) $this->getNormalizedValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getMinValue()
	 */
	public function getMinValue() : float
	{
		return (float) 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getMaxValue()
	 */
	public function getMaxValue() : float
	{
		return (float) 1;
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getCurrentValue()
	 */
	public function getCurrentValue() : float
	{
		return (float) $this->_valid;
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getNormalizedValue()
	 */
	public function getNormalizedValue() : float
	{
		return $this->getCurrentValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::comparesTo()
	 */
	public function comparesTo(ScoreInterface $score) : int
	{
		return (int) ($this->getNormalizedValue() - $score->getNormalizedValue());
	}
}
