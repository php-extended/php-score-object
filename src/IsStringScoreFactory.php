<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * IsStringScoreFactory class file.
 * 
 * This class is a factory that builds IsStringScore objects.
 * 
 * @author Anastaszor
 */
class IsStringScoreFactory implements ScoreFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Score\ScoreFactoryInterface::createScore()
	 */
	public function createScore(array $values = []) : ScoreInterface
	{
		$values = \array_values($values);
		$actual = $values[0] ?? null;
		
		return new IsStringScore($actual);
	}
	
}
