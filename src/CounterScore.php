<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * CounterScore class file.
 * 
 * This class acts as a counter which can be incremented two ways. The first
 * way increments the counter by one (and the total to maintain the ratio) and
 * the second way increments only the maximum, but not the total.
 * 
 * @author Anastaszor
 */
class CounterScore extends IntegerScore
{
	
	/**
	 * Builds a new CounterScore with the current count.
	 * 
	 * @param integer $currentCount
	 */
	public function __construct($currentCount = 0)
	{
		parent::__construct(0, $currentCount, $currentCount);
	}
	
	/**
	 * Increments the current count for this score. This will increase the
	 * count/total ratio.
	 */
	public function incrementCount() : void
	{
		$this->_current++;
		$this->incrementTotal();
	}
	
	/**
	 * Increments the total count for this score. This will decrease the
	 * count/total ratio.
	 */
	public function incrementTotal() : void
	{
		$this->_max++;
	}
	
}
