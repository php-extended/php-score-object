<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

use DateTimeInterface;

/**
 * DateYearEqualsScore class file.
 *
 * This class represents a score which is equal to 100% if the expected and the
 * actual values are both datetimes or nulls and are equal to each other for
 * their year part, and 0% of one of the values is not a datetime or null, or
 * if they do not equal each other on their year part.
 *
 * @author Anastaszor
 */
class DateYearEqualsScore extends BooleanScore
{
	
	/**
	 * Builds a new Date Year Equals Score from the expected and given values.
	 *
	 * @param ?DateTimeInterface $expected
	 * @param ?DateTimeInterface $actual
	 */
	public function __construct(?DateTimeInterface $expected, ?DateTimeInterface $actual)
	{
		$dtExpected = (null === $expected ?: $expected->format('Y'));
		$dtActual = (null === $actual ?: $actual->format('Y'));
		parent::__construct($dtExpected === $dtActual);
	}
	
}
