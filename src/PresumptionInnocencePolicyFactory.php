<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * PresumptionInnocencePolicyFactory class file.
 * 
 * This class is a factory that builds PresumptionInnoncencePolicy objects.
 * 
 * @author Anastaszor
 */
class PresumptionInnocencePolicyFactory implements ScorePolicyFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Score\ScorePolicyFactoryInterface::createScorePolicy()
	 */
	public function createScorePolicy() : ScorePolicyInterface
	{
		return new PresumptionInnocencePolicy();
	}
	
}
