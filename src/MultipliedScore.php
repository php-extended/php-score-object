<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * MultipliedScore class file.
 * 
 * This class is a composite score which multiplies the normalized values of
 * the given scores to make a new score.
 * 
 * @author Anastaszor
 */
class MultipliedScore extends FloatScore
{
	
	/**
	 * Builds a new MultipliedScore with the given other scores.
	 * 
	 * @param ScoreInterface $score1
	 * @param ScoreInterface $score2
	 */
	public function __construct(ScoreInterface $score1, ScoreInterface $score2)
	{
		parent::__construct(0, 1, $score1->getNormalizedValue() * $score2->getNormalizedValue());
	}
	
}
