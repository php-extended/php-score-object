<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * BooleanNotNullEqualsScoreFactory class file.
 * 
 * This class is a factory that builds BooleanNotNullEqualsScore objects.
 * 
 * @author Anastaszor
 */
class BooleanNotNullEqualsScoreFactory implements ScoreFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Score\ScoreFactoryInterface::createScore()
	 */
	public function createScore(array $values = []) : ScoreInterface
	{
		$values = \array_values($values);
		$expected = $values[0] ?? null;
		$actual = $values[1] ?? null;
		if(!\is_bool($expected))
		{
		$expected = null;
		}
		if(!\is_bool($actual))
		{
		$actual = null;
		}
		
		return new BooleanNotNullEqualsScore($expected, $actual);
	}
	
}
