<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * StringEqualsScore class file.
 *
 * This class represents a score which is equal to 100% if the expected and the
 * actual values are both strings or nulls and are equal to each other, and
 * 0% if one of the values is not an integer or null, or if they do not equal
 * each other for the === operator.
 *
 * @author Anastaszor
 */
class StringEqualsScore extends BooleanScore
{
	
	/**
	 * Builds a new StringEqualsScore from the expected and actual values.
	 *
	 * @param ?string $expected
	 * @param ?string $actual
	 */
	public function __construct(?string $expected, ?string $actual)
	{
		parent::__construct($expected === $actual);
	}
	
}
