<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * IsFloatScore class file.
 * 
 * This class represents a score which is equal to 100% if the given value is
 * a float or null, and to 0% if the given value is not a float nor null.
 * 
 * @author Anastaszor
 */
class IsFloatScore extends BooleanScore
{
	
	/**
	 * Builds a new IsFloatScore from the actual value.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $actual
	 */
	public function __construct($actual)
	{
		parent::__construct(\is_float($actual) || null === $actual);
	}
	
}
