<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * MaximumScorePolicy class file.
 * 
 * This class is made to get the maximum score amonsgt all of those which are
 * given to this policy.
 * 
 * @author Anastaszor
 */
class MaximumScorePolicy implements ScorePolicyInterface
{
	
	/**
	 * The maximum normalized value.
	 * 
	 * @var float
	 */
	protected float $_max = 0.0;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScorePolicyInterface::absorb()
	 */
	public function absorb(ScoreInterface $score, int $weight = 1) : bool
	{
		$this->_max = \max($this->_max, $score->getNormalizedValue());
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScorePolicyInterface::getCurrentValue()
	 */
	public function getCurrentValue() : ScoreInterface
	{
		return new FloatScore(0, 1, $this->_max);
	}
	
}
