<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

use Stringable;

/**
 * ScoreCollectionNode class file.
 *
 * This class is just a filler to be able to store scores and weight into the
 * data structure of the Score Collection.
 *
 * @author Anastaszor
 */
class ScoreCollectionNode implements Stringable
{
	
	/**
	 * The score of the node.
	 *
	 * @var ScoreInterface
	 */
	protected ScoreInterface $_score;
	
	/**
	 * The weight of the node.
	 *
	 * @var integer
	 */
	protected int $_weight = 1;
	
	/**
	 * Builds a new Score Collection Node.
	 *
	 * @param ScoreInterface $score
	 * @param integer $weight
	 */
	public function __construct(ScoreInterface $score, int $weight = 1)
	{
		$this->_score = $score;
		$this->_weight = $weight;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the score of the node.
	 *
	 * @return ScoreInterface
	 */
	public function getScore() : ScoreInterface
	{
		return $this->_score;
	}
	
	/**
	 * Gets the weight of the node.
	 *
	 * @return integer
	 */
	public function getWeight() : int
	{
		return $this->_weight;
	}
	
}
