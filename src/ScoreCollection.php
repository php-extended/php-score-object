<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * ScoreCollection class file.
 *
 * This class is a basic implementation of the ScoreCollectionInterface.
 *
 * @author Anastaszor
 */
class ScoreCollection implements ScoreCollectionInterface
{
	
	/**
	 * All the nodes inside this collection.
	 *
	 * @var array<integer, ScoreCollectionNode>
	 */
	protected array $_nodes = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreCollectionInterface::add()
	 */
	public function add(ScoreInterface $score, int $weight = 1) : bool
	{
		$this->_nodes[] = new ScoreCollectionNode($score, $weight);
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreCollectionInterface::resolve()
	 */
	public function resolve(ScorePolicyInterface $policy) : ScoreInterface
	{
		foreach($this->_nodes as $node)
		{
			$policy->absorb($node->getScore(), $node->getWeight());
		}
		
		return $policy->getCurrentValue();
	}
	
}
