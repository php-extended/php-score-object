<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

use Exception;
use InvalidArgumentException;

/**
 * OrderedListScore class file.
 *
 * This class compares two ordered lists, the expected and the actual, to make
 *
 * @author Anastaszor
 */
class OrderedListScore implements ScoreInterface
{
	
	/**
	 * The inner score compacted from the list.
	 *
	 * @var ScoreInterface
	 */
	protected ScoreInterface $_innerScore;
	
	/**
	 * Builds a new OrderedListScore based on the given policy and score classes
	 * and the given data.
	 *
	 * @param class-string $comparatorClassname the class name of the ScorePolicyInterface
	 * @param class-string $scoreClassname the class name of the ScoreInterface
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $expected the expected data
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $actual the actual data to score against
	 * @throws Exception if the $score_classname does not have a constructor
	 *                   __construct($expected, $actual) with the right class type hints
	 * @throws InvalidArgumentException if the class names refers to object
	 *                                  that does not exists, or that does not implements the right interfaces
	 */
	public function __construct(string $comparatorClassname, string $scoreClassname, array $expected, array $actual)
	{
		if(!\class_exists($comparatorClassname))
		{
			$message = 'The class name "{name}" does not exists.';
			$context = ['{name}' => $comparatorClassname];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(!\is_subclass_of($comparatorClassname, ScorePolicyInterface::class))
		{
			$message = 'The class name "{name}" is not an instance of "{interface}".';
			$context = ['{name}' => $comparatorClassname, '{interface}' => ScorePolicyInterface::class];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(!\class_exists($scoreClassname))
		{
			$message = 'The class name "{name}" does not exists.';
			$context = ['{name}' => $scoreClassname];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(!\is_subclass_of($scoreClassname, ScoreInterface::class))
		{
			$message = 'The class name "{name}" is not an instance of "{interface}".';
			$context = ['{name}' => $scoreClassname, '{interface}' => ScoreInterface::class];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		$policy = new $comparatorClassname();
		
		$expected = \array_values($expected);	// reset indexes
		$actual = \array_values($actual);		// reset indexes
		
		for($i = 0; \max(\count($expected), \count($actual)) > $i; $i++)
		{
			$first = $expected[$i] ?? null;
			$second = $actual[$i] ?? null;
			$policy->absorb(new $scoreClassname($first, $second));
		}
		$this->_innerScore = $policy->getCurrentValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return (string) $this->getNormalizedValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getMinValue()
	 */
	public function getMinValue() : float
	{
		return $this->_innerScore->getMinValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getMaxValue()
	 */
	public function getMaxValue() : float
	{
		return $this->_innerScore->getMaxValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getCurrentValue()
	 */
	public function getCurrentValue() : float
	{
		return $this->_innerScore->getCurrentValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getNormalizedValue()
	 */
	public function getNormalizedValue() : float
	{
		return $this->_innerScore->getNormalizedValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::comparesTo()
	 */
	public function comparesTo(ScoreInterface $score) : int
	{
		return $this->_innerScore->comparesTo($score);
	}
	
}
