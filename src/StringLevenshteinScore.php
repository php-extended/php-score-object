<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * StringLevenshteinScore class file.
 *
 * This class represents the score obtained by comparing a string to a reference
 * string and counting the difference with the levenshtein distance.
 *
 * @author Anastaszor
 */
class StringLevenshteinScore implements ScoreInterface
{
	
	/**
	 * The minimal value.
	 *
	 * @var float
	 */
	protected float $_min = 0;
	
	/**
	 * The maximal value.
	 *
	 * @var float
	 */
	protected float $_max = 1;
	
	/**
	 * The current value.
	 *
	 * @var float
	 */
	protected float $_current = 1;
	
	/**
	 * Builds a new String Levenshtein Score from expected and actual strings.
	 *
	 * @param ?string $expected
	 * @param ?string $actual
	 */
	public function __construct(?string $expected, ?string $actual)
	{
		if(null === $expected)
		{
			if(null === $actual)
			{
				$this->_min = 0;
				$this->_max = 1;
				$this->_current = 1;
				
				return;
			}
			$this->_min = 0;
			$this->_max = 1;
			$this->_current = 0;
			
			return;
		}
		
		if(null === $actual)
		{
			$this->_min = 0;
			$this->_max = (int) \mb_strlen($expected);
			$this->_current = 0;
			
			return;
		}
		
		$this->_min = 0;
		$this->_max = (int) \max(\mb_strlen($actual), \mb_strlen($expected));
		$this->_current = $this->_max - \str_levenshtein($expected, $actual);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return (string) $this->getNormalizedValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getMinValue()
	 */
	public function getMinValue() : float
	{
		return $this->_min;
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getMaxValue()
	 */
	public function getMaxValue() : float
	{
		return $this->_max;
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getCurrentValue()
	 */
	public function getCurrentValue() : float
	{
		return $this->_current;
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getNormalizedValue()
	 */
	public function getNormalizedValue() : float
	{
		if($this->_max === $this->_min)
		{
			return $this->_min;
		}
		
		return ($this->_current - $this->_min) / ($this->_max - $this->_min);
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::comparesTo()
	 */
	public function comparesTo(ScoreInterface $score) : int
	{
		return (int) ($this->getNormalizedValue() - $score->getNormalizedValue());
	}
	
}
