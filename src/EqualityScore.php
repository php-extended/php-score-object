<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * EqualityScore class file.
 * 
 * This class is a generic comparator between two objects that scores if the
 * two objects are equals. This score is equal to 100% if both the expected and
 * the actual values are equals, and is equal to 0% if both values are not 
 * equals for the sense of the === operator.
 * 
 * @author Anastaszor
 */
class EqualityScore extends BooleanScore
{
	
	/**
	 * Builds a new Equality Score with the expected and the given value.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $expected
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $actual
	 */
	public function __construct($expected, $actual)
	{
		parent::__construct($expected === $actual);
	}
	
}
