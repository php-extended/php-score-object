<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * BooleanScore class file.
 *
 * This class represents a score which is equal to 100% if the expected and
 * the actual values are both booleans or nulls and are equals to each other,
 * and 0% if one of the values is not a boolean or null, or if they do not
 * equal each other for the === operator. 
 *
 * @author Anastaszor
 */
class BooleanEqualsScore extends BooleanScore
{
	
	/**
	 * Builds a new BooleanEqualsScore from the expected and actual values.
	 *
	 * @param ?boolean $expected
	 * @param ?boolean $actual
	 */
	public function __construct(?bool $expected, ?bool $actual)
	{
		parent::__construct($expected === $actual);
	}
	
}
