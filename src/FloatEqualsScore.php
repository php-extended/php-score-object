<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * FloatEqualsScore class file.
 * 
 * This class represents a score which is equal to 100% if the expected and
 * the actual values are both floats or nulls and are equals to each other,
 * and 0% if one of the values is not a boolean or null, or if they do not
 * equal to each other for the === operator.
 * 
 * @author Anastaszor
 */
class FloatEqualsScore extends BooleanScore
{
	
	/**
	 * Builds a new FloatEqualsScore with the given expected and actual values.
	 * 
	 * @param ?float $expected
	 * @param ?float $actual
	 */
	public function __construct(?float $expected, ?float $actual)
	{
		parent::__construct($expected === $actual);
	}
	
}
