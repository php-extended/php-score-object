<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * IsBooleanNotNullScore class file.
 * 
 * This class represents a score which is equal to 100% if the given value is
 * a boolean not null, and to 0% if the given value is not a boolean or is null.
 * 
 * @author Anastaszor
 */
class IsBooleanNotNullScore extends BooleanScore
{
	
	/**
	 * Builds a new IsBooleanNotNullScore from the actual value.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $actual
	 */
	public function __construct($actual)
	{
		parent::__construct(true === $actual || false === $actual);
	}
	
}
