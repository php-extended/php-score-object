<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * FloatScore class file.
 *
 * This is a simple implementation of the ScoreInterface interface.
 *
 * @author Anastaszor
 */
class FloatScore implements ScoreInterface
{
	
	/**
	 * The minimal value.
	 *
	 * @var float
	 */
	protected $_min = 0;
	
	/**
	 * The maximal value.
	 *
	 * @var float
	 */
	protected $_max = 1;
	
	/**
	 * The current value.
	 *
	 * @var float
	 */
	protected $_current = 0;
	
	/**
	 * Builds a new Numeric Score.
	 *
	 * @param float $min
	 * @param float $max
	 * @param float $current
	 */
	public function __construct(float $min, float $max, float $current)
	{
		$this->_min = $min;
		$this->_max = $max;
		$this->_current = $current;
		
		if($this->_min > $this->_max)
		{
			$temp = $this->_min;
			$this->_min = $this->_max;
			$this->_max = $temp;
		}
		if($this->_current > $this->_max)
		{
			$temp = $this->_current;
			$this->_current = $this->_max;
			$this->_max = $temp;
		}
		if($this->_current < $this->_min)
		{
			$temp = $this->_current;
			$this->_current = $this->_min;
			$this->_min = $temp;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return (string) $this->getNormalizedValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getMinValue()
	 * @return float
	 */
	public function getMinValue() : float
	{
		return $this->_min;
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getMaxValue()
	 * @return float
	 */
	public function getMaxValue() : float
	{
		return $this->_max;
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getCurrentValue()
	 * @return float
	 */
	public function getCurrentValue() : float
	{
		return $this->_current;
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::getNormalizedValue()
	 * @return float
	 */
	public function getNormalizedValue() : float
	{
		if(0.0 === ((float) ($this->getMaxValue() - $this->getMinValue())))
		{
			return 0;
		}
		
		return ($this->getCurrentValue() - $this->getMinValue()) / ($this->getMaxValue() - $this->getMinValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see ScoreInterface::comparesTo()
	 * @return integer
	 */
	public function comparesTo(ScoreInterface $score) : int
	{
		return (int) ($this->getNormalizedValue() - $score->getNormalizedValue());
	}
	
}
