<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

use DateTimeInterface;

/**
 * DateMonthEqualsScore class file.
 *
 * This class represents a score which is equal to 100% if the expected and the
 * actual values are both datetimes or nulls and they are equal to each other
 * for their year-month part, and 0% if one of the values is not a datetime or
 * null, or if they do not equal each other on their year-month part.
 *
 * @author Anastaszor
 */
class DateMonthEqualsScore extends BooleanScore
{
	
	/**
	 * Builds a new Date Month Equals Score from the expected and given values.
	 *
	 * @param ?DateTimeInterface $expected
	 * @param ?DateTimeInterface $actual
	 */
	public function __construct(?DateTimeInterface $expected, ?DateTimeInterface $actual)
	{
		$dtExpected = (null === $expected ?: $expected->format('Y-m'));
		$dtActual = (null === $actual ?: $actual->format('Y-m'));
		parent::__construct($dtExpected === $dtActual);
	}
	
}
