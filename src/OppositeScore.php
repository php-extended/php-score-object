<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

/**
 * OppositeScore class file.
 * 
 * This class is to opposite the score on the [0-1] normalized scale. A given
 * score of 0 will become 1, and a given score of 1 will become 0, and a given
 * score of 0.6 will become 1-0.6 = 0.4.
 * 
 * @author Anastaszor
 */
class OppositeScore extends FloatScore
{
	
	/**
	 * Gets the inverted score of the given score (on a [0-1] scale).
	 * 
	 * @param ScoreInterface $score
	 */
	public function __construct(ScoreInterface $score)
	{
		parent::__construct(0, 1, 1.0 - $score->getNormalizedValue());
	}
	
}
