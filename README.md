# php-extended/php-score-object
A library that implements the php-score-interface package

![coverage](https://gitlab.com/php-extended/php-score-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-score-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-score-object ^8`


## Basic Usage

You may use this library the following way :

```php

use PhpExtended\Score\StringLevenshteinScore;
use PhpExtended\Score\ScoreCollection;
use PhpExtended\Score\PresumptionGuiltPolicy;

// First, let's image you have three strings and a reference string, and you
// want to know which is the global distance of the three strings from the
// reference

$reference = '<put your reference string here>';
$test1     = '<put your first test string here>';
$test2     = '<put your second test string here>';
$test3     = '<put your third test string here>';

$score1 = new StringLevenshteinScore($reference, $test1);
$score2 = new StringLevenshteinScore($reference, $test2);
$score3 = new StringLevenshteinScore($reference, $test3);

// Next, concatenate the results
$collection = new ScoreCollection();
$collection->add($score1);
$collection->add($score2);
$collection->add($score3);

// Finally, get the mean value
$result = $collection->resolve(new PresumptionGuiltPolicy());

// To get the result as a number between 0 and 1, use the getNormalizedValue()
// of the IScore object $result.

```


## License

MIT (See [license file](LICENSE)).
