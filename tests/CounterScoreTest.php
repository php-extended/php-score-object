<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\CounterScore;
use PHPUnit\Framework\TestCase;

/**
 * CounterScoreTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Score\CounterScore
 *
 * @internal
 *
 * @small
 */
class CounterScoreTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CounterScore
	 */
	protected CounterScore $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('0', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CounterScore();
	}
	
}
