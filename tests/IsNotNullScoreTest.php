<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\IsNotNullScore;
use PHPUnit\Framework\TestCase;

/**
 * IsNotNullScoreTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Score\IsNotNullScore
 *
 * @internal
 *
 * @small
 */
class IsNotNullScoreTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IsNotNullScore
	 */
	protected IsNotNullScore $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('0', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IsNotNullScore(null);
	}
	
}
