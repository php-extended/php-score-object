<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\IsNotNullScoreFactory;
use PHPUnit\Framework\TestCase;

/**
 * IsNotNullScoreFactoryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Score\IsNotNullScoreFactory
 *
 * @internal
 *
 * @small
 */
class IsNotNullScoreFactoryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IsNotNullScoreFactory
	 */
	protected IsNotNullScoreFactory $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IsNotNullScoreFactory();
	}
	
}
