<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\StringEqualsScore;
use PHPUnit\Framework\TestCase;

/**
 * StringEqualsScoreTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Score\StringEqualsScore
 *
 * @internal
 *
 * @small
 */
class StringEqualsScoreTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StringEqualsScore
	 */
	protected StringEqualsScore $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('0', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StringEqualsScore('expected', 'actual');
	}
	
}
