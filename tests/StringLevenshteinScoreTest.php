<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\StringLevenshteinScore;
use PHPUnit\Framework\TestCase;

/**
 * StringLevenshteinScoreTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Score\StringLevenshteinScore
 *
 * @internal
 *
 * @small
 */
class StringLevenshteinScoreTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StringLevenshteinScore
	 */
	protected StringLevenshteinScore $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('0.125', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StringLevenshteinScore('expected', 'actual');
	}
	
}
