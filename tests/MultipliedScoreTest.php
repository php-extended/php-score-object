<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\BooleanScore;
use PhpExtended\Score\MultipliedScore;
use PHPUnit\Framework\TestCase;

/**
 * MultipliedScoreTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Score\MultipliedScore
 *
 * @internal
 *
 * @small
 */
class MultipliedScoreTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MultipliedScore
	 */
	protected MultipliedScore $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('1', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MultipliedScore(new BooleanScore(true), new BooleanScore(true));
	}
	
}
