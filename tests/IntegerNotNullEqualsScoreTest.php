<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\IntegerNotNullEqualsScore;
use PHPUnit\Framework\TestCase;

/**
 * IntegerNotNullEqualsScoreTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Score\IntegerNotNullEqualsScore
 *
 * @internal
 *
 * @small
 */
class IntegerNotNullEqualsScoreTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IntegerNotNullEqualsScore
	 */
	protected IntegerNotNullEqualsScore $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('1', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IntegerNotNullEqualsScore(12, 12);
	}
	
}
