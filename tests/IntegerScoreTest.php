<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\IntegerScore;
use PHPUnit\Framework\TestCase;

/**
 * IntegerScoreTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Score\IntegerScore
 *
 * @internal
 *
 * @small
 */
class IntegerScoreTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IntegerScore
	 */
	protected IntegerScore $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('1', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IntegerScore(1, 12, 12);
	}
	
}
