<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\BooleanScore;
use PhpExtended\Score\MinimumScore;
use PHPUnit\Framework\TestCase;

/**
 * MinimumScoreTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Score\MinimumScore
 *
 * @internal
 *
 * @small
 */
class MinimumScoreTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MinimumScore
	 */
	protected MinimumScore $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('1', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MinimumScore(new BooleanScore(true), new BooleanScore(true));
	}
	
}
