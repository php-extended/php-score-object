<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\BooleanScore;
use PhpExtended\Score\OppositeScore;
use PHPUnit\Framework\TestCase;

/**
 * OppositeScoreTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Score\OppositeScore
 *
 * @internal
 *
 * @small
 */
class OppositeScoreTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var OppositeScore
	 */
	protected OppositeScore $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('0', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new OppositeScore(new BooleanScore(true));
	}
	
}
