<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\BooleanScore;
use PhpExtended\Score\OrderedListScore;
use PhpExtended\Score\PresumptionGuiltPolicy;
use PHPUnit\Framework\TestCase;

/**
 * OrderedListScoreTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Score\OrderedListScore
 *
 * @internal
 *
 * @small
 */
class OrderedListScoreTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var OrderedListScore
	 */
	protected OrderedListScore $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('0', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new OrderedListScore(PresumptionGuiltPolicy::class, BooleanScore::class, [], []);
	}
	
}
